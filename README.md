Script que inicia una base de datos minima del tipo `<clave> : <valor>`
Ambos tipos son Strings.

Instrucciones:
*Se requiere python3.5

en consola:

`~$ python NetClient.py`

Para integrarlo con un script, se requiere usar:
```python
from SDBApi import SDBApi
server = SDBApi()
```
con esto se crea una instancia del cliente.

Por hacer:

1)Autoguardado.

2)Que el cliente no se pare si el servidor cae.

3)Agregar opción para especificar el archivo de la base de datos.

4)Permitir multiples sesiones.

5)Integracion con otros lenguajes.